
extern int  opterr,             /* if error message should be printed */
optind,             /* index into parent argv vector */
optopt,                 /* character checked for validity */
optreset;               /* reset getopt */
extern char    *optarg;                /* argument associated with option */

#define BADCH   (int)'?'
#define BADARG  (int)':'

#define EMSG    ""

int getopt(int nargc, char * const nargv[], const char *ostr);
//class stdargs
//{
//public:
//	stdargs();
//	~stdargs();
//};

