﻿

#include <iostream>
#include <limits>
#include <fstream>
#include "LinkPredictor.h"

using std::numeric_limits;
using std::cout;
using std::cerr;
using std::scientific;
using std::ios_base;
using std::ostream;

LinkPredictor::LinkPredictor( const WeightedNetwork& network ) : network(network), vertex(INVALID_VERTEX), neighbor(INVALID_VERTEX) {
}

LinkPredictor::~LinkPredictor() {
}
/*
نتایج پیش‌بینی را چاپ می‌کند
*/
void LinkPredictor::printOutNeighborScores( unsigned int degree,const char* outputFilename) {
	// if the degree is 0, do all-pairs predictions
	std::string filename(outputFilename);
	filename.append(EXTENSTION_PREDICTION);
	std::ofstream outfile(filename);
	int count = 0;
	if( degree == 0 ) 
	{
		for( vertex_t vertex = 0; vertex < network.vertexCount(); ++vertex )
		{
			for( vertex_t neighbor = 0; neighbor < network.vertexCount(); ++neighbor )
			{
				if( vertex != neighbor && !network.hasEdge( vertex, neighbor ) )
				{
					// DO WE PREDICT LINKS THAT ALREADY EXIST? - // DO PROVIDED NETWORKS INCLUDE LINKS IN BOTH DIRECTIONS
					double predictedValue = this->generateScore( vertex, neighbor );
					unsigned int vertexName = this->network.translateIntToExt( vertex );
					unsigned int neighborName = this->network.translateIntToExt( neighbor );
					outfile << vertexName << " " << neighborName << " " << predictedValue << "\n";
					count++;
				}
			}
			cout << count << std::endl;
		}
		outfile.close();
		return;
	}
	for( vertex_t vertex = 0; vertex < network.vertexCount(); vertex++ )
	{
		vector<vertex_t> verticesToPredict;
		verticesToPredict = this->network.findOutNeighbors( vertex, degree );
		for( vector<unsigned int>::const_iterator neighborIterator = verticesToPredict.begin(); neighborIterator != verticesToPredict.end(); neighborIterator++ ) 
		{
			vertex_t neighbor = *neighborIterator;
			double predictedValue = this->generateScore( vertex, neighbor );
			unsigned int vertexName = this->network.translateIntToExt( vertex );
			unsigned int neighborName = this->network.translateIntToExt( neighbor );
			outfile << vertexName << " " << neighborName << " " << predictedValue << "\n";
			count++;
		}
		cout << count << std::endl;
	}
	outfile.close();
}

void LinkPredictor::printInNeighborScores( unsigned int degree, const char* outputFilename) {
	
	std::string filename(outputFilename);
	filename.append(EXTENSTION_PREDICTION);
	std::ofstream outfile(filename);
	int count = 0;
	//std::ostringstream stringStream;
	
	// if the degree is 0, do all-pairs predictions
	if( degree == 0 ) 
	{
		for( vertex_t vertex = 0; vertex < network.vertexCount(); ++vertex ) 
		{
			for( vertex_t neighbor = 0; neighbor < network.vertexCount(); ++neighbor )
			{
				if( vertex != neighbor && !network.hasEdge( neighbor, vertex ) ) 
				{
					// DO WE PREDICT LINKS THAT ALREADY EXIST? - // DO PROVIDED NETWORKS INCLUDE LINKS IN BOTH DIRECTIONS
					double predictedValue = this->generateScore( vertex, neighbor );
					unsigned int vertexName = this->network.translateIntToExt( vertex );
					unsigned int neighborName = this->network.translateIntToExt( neighbor );
					outfile << vertexName << " " << neighborName << " " << predictedValue << "\n";
					count++;
				}
			}
			cout << count << std::endl;
		}
		outfile.close();
		return;
	}
	for( vertex_t vertex = 0; vertex < network.vertexCount(); vertex++ )
	{
		vector<vertex_t> verticesToPredict;
		verticesToPredict = this->network.findInNeighbors( vertex, degree );
		for( vector<unsigned int>::const_iterator neighborIterator = verticesToPredict.begin(); neighborIterator != verticesToPredict.end(); neighborIterator++ ) {
			vertex_t neighbor = *neighborIterator;
			double predictedValue = this->generateScore( vertex, neighbor );
			unsigned int vertexName = this->network.translateIntToExt( vertex );
			unsigned int neighborName = this->network.translateIntToExt( neighbor );
			//std::ostream sss;
			
		
			//std::string copyOfStr = stringStream.str();
			outfile << vertexName << " " << neighborName << " " << predictedValue << "\n";
			count++;
			
		}
		cout << count << std::endl;
	}
	outfile.close();
	}
