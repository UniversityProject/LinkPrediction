#ifndef JACCARDCOEFFICIENTLINKPREDICTOR_H
#define JACCARDCOEFFICIENTLINKPREDICTOR_H

#include "WeightedNetwork.h"
#include "LinkPredictor.h"

class JaccardCoefficientLinkPredictor : public LinkPredictor {
	private:
	protected:
	public:
		JaccardCoefficientLinkPredictor( const WeightedNetwork& );
		~JaccardCoefficientLinkPredictor();
		double generateScore( vertex_t, vertex_t );
};

#endif
