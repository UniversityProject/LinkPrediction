
#ifdef LINKPREDICTOR_EXPORTS
#define LINKPREDICTOR_API __declspec(dllexport) 
#else
#define LINKPREDICTOR_API __declspec(dllimport) 
#endif


#ifndef LINKPREDICTOR_H
#define LINKPREDICTOR_H

#include <vector>
#include <string.h>
#include "WeightedNetwork.h"

using std::ostream;
using std::vector;

class LinkPredictor {
	private:
		LinkPredictor& operator=( const LinkPredictor& ); ////Learn
		const char * EXTENSTION_PREDICTION = ".pred";
	protected:
		const WeightedNetwork& network;
		vertex_t vertex;
		vertex_t neighbor;
		LinkPredictor();
	public:
		LINKPREDICTOR_API enum Direction { IN, OUT };
		LINKPREDICTOR_API LinkPredictor( const WeightedNetwork& );
		LINKPREDICTOR_API virtual ~LinkPredictor();
		LINKPREDICTOR_API virtual double generateScore( vertex_t, vertex_t ) = 0;  //Learn
		LINKPREDICTOR_API void printOutNeighborScores( unsigned int degree, const char* outputFilename);
		LINKPREDICTOR_API void printInNeighborScores( unsigned int degree, const char* outputFilename);
};

#endif
