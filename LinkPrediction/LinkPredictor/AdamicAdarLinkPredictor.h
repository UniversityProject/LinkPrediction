

#ifdef ADAMICADARLINKPREDICTOR_EXPORTS
#define ADAMICADARLINKPREDICTOR_API __declspec(dllexport) 
#else
#define ADAMICADARLINKPREDICTOR_API __declspec(dllimport) 
#endif

#ifndef ADAMICADARLINKPREDICTOR_H
#define ADAMICADARLINKPREDICTOR_H

#include "WeightedNetwork.h"
#include "LinkPredictor.h"

class AdamicAdarLinkPredictor : public LinkPredictor {
	private:
	protected:
	public:
		ADAMICADARLINKPREDICTOR_API AdamicAdarLinkPredictor( const WeightedNetwork& );
		ADAMICADARLINKPREDICTOR_API ~AdamicAdarLinkPredictor();
		ADAMICADARLINKPREDICTOR_API virtual double generateScore( vertex_t, vertex_t );
};

#endif
