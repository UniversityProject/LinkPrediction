
#include <math.h>
#include "AdamicAdarLinkPredictor.h"

AdamicAdarLinkPredictor::AdamicAdarLinkPredictor( const WeightedNetwork& network ) : LinkPredictor(network) {
}

AdamicAdarLinkPredictor::~AdamicAdarLinkPredictor() {
}

double AdamicAdarLinkPredictor::generateScore( vertex_t vertex, vertex_t neighbor ) {
	double result = 0.0;
	neighbor_set_t commonNeighbors = this->network.commonOutNeighbors( vertex, neighbor );
	for( neighbor_set_t::const_iterator cnIterator = commonNeighbors.begin(); cnIterator != commonNeighbors.end(); cnIterator++ ) {
		const vertex_t commonNeighbor = cnIterator->first;
		result += 1 / log( this->network.inDegree( commonNeighbor ) );
	}
	return result;
}
