﻿#include <iostream>
#include <fstream>
#include <time.h>

#include "stdargs.h"
#include "WeightedNetwork.h"
#include "LinkPredictor\LinkPredictor.h"
#include "LinkPredictor\AdamicAdarLinkPredictor.h"
#include "LinkPredictor\CommonNeighborLinkPredictor.h"
#include "LinkPredictor\JaccardCoefficientLinkPredictor.h"
#include "LinkPredictor\PreferentialAttachmentLinkPredictor.h"


using namespace std;

bool verbose = false;

void display_time(const char *str) {
	time_t rawtime;
	time(&rawtime);
	cerr << str << ": " << ctime(&rawtime);
}

void usage(const char* binary) {
	cerr << "Usage: " << binary << " [options] predictor\n";
	cerr << "Options: -h             Print this usage message and exit\n";
	cerr << "         -d DIRECTION   The direction of edges to use; must be in {I,O}\n";
	cerr << "         -f NETWORK     File containing the network in which to predict\n";
	cerr << "         -n DISTANCE    Degree of neighborhood in which to predict\n";
	cerr << "         -c             Specifies that node-pairs for prediction are available at standard input\n";
	cerr << "         -v              verbose" << endl;
}


int main(int argc, char* argv[]) {
	cout << "Start link prediction" << endl;
	char c;
	bool specifiedPredictions = false;
	bool outEdges = true;
	unsigned int degree = 2;
	ifstream file;
	cout << "Checking Arguments" << endl;
	while ((c = getopt(argc, argv, "+hcd:f:n:")) >= 0) {
		if (c == 'd')
		{
			if (strcmp(optarg, "I") == 0)
			{
				outEdges = false;
			}
		}
		else if (c == 'f')
		{
			file.open(optarg, ios::in);
		}
		else if (c == 'n')
		{
			degree = atoi(optarg);

		}
		else if (c == 'c')
		{
			specifiedPredictions = true;
		}
		else if (c == 'h')
		{
			usage(argv[0]);
			exit(0);
		}
		else if (c == 'v')
		{
			verbose = true;
		}
	}
	// این شرط برای مشخص کردن نام متد پیش‌بینی لینک می‌باشد
	// در هر صورت «چه کاربر آرگومانی وارد نماید و چه آرگومانی وارد نکند» پارامتر اول (یعنی عنصر صفرم argv) نام برنامه خواهد بود
	//برابر با یک خواهد بود.
	// optind : شماره آرگومان ورودی در argv
	if (argc < optind + 1) {
		usage(argv[0]);
		return 1;
	}

	if (verbose)
	{
		display_time("  start computation");
		cout << endl;
	}
	const char* predictorName = argv[optind++];

	WeightedNetwork network = WeightedNetwork::readNetwork(file);
	if (!outEdges) {
		network = network.reverseEdges();
	}
	LinkPredictor* predictor = NULL;
	if (strcmp(predictorName, "AdamicAdar") == 0) {
		predictor = new AdamicAdarLinkPredictor(network);
	}
	else if (strcmp(predictorName, "CommonNeighbor") == 0) {
		predictor = new CommonNeighborLinkPredictor(network);
	}
	else if (strcmp(predictorName, "JaccardCoefficient") == 0) {
		predictor = new JaccardCoefficientLinkPredictor(network);
	}
	else if (strcmp(predictorName, "PreferentialAttachment") == 0) {
		predictor = new PreferentialAttachmentLinkPredictor(network);
	}
	else {
		cerr << "invalid link predictor: " << predictorName << "\n";
		return 1;
	}

	cout.precision(15);
	if (specifiedPredictions) {
		vertex_t vertex;
		vertex_t neighbor;
		while (cin >> vertex >> neighbor) {
			vertex_t internalVertex = network.translateExtToInt(vertex);
			vertex_t internalNeighbor = network.translateExtToInt(neighbor);
			if (internalVertex != INVALID_VERTEX && internalNeighbor != INVALID_VERTEX) {
				cout << vertex << " " << neighbor << " " << predictor->generateScore(internalVertex, internalNeighbor);
			}
		}
	}
	else {
		if (!outEdges) {
			predictor->printInNeighborScores(degree, predictorName);
		}
		else {
			predictor->printOutNeighborScores(degree, predictorName);
		}
	}
	delete predictor;
	if (verbose)
	{
		display_time("  end computation");
		cout << endl;
	}

	return 0;

}