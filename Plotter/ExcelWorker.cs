﻿using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plotter
{
   public class ExcelWorker
    {
       

        public ExcelWorker()
        {
            
        }
        public static void CreateExcelFile()
        {
           
            string [] dts = File.ReadAllLines(@"C:\Users\Alias\Desktop\LinkPrediction\Plotter\AdamicAdar.pr");

            Microsoft.Office.Interop.Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();

            if (xlApp == null)
            {
                Console.WriteLine("EXCEL could not be started. Check that your office installation and project references are correct.");
                return;
            }
            xlApp.Visible = true;

            Workbook wb = xlApp.Workbooks.Add(1);
            Worksheet ws = (Worksheet)wb.Worksheets[1];

            if (ws == null)
            {
                Console.WriteLine("Worksheet could not be created. Check that your office installation and project references are correct.");
            }
            Worksheet xlWorkSheet = (Worksheet)wb.Worksheets.get_Item(1);

            for (int i = 0; i < dts.Length; i++)
            {
                string[] temp = dts[i].Split(null);
                var one = Convert.ToDouble(temp[0], CultureInfo.InvariantCulture);
                var two = Convert.ToDouble(temp[1], CultureInfo.InvariantCulture);
                xlWorkSheet.Cells[i+1, 1] = one;
                xlWorkSheet.Cells[i+1, 2] = two;
            }
            
        }
    }
}
