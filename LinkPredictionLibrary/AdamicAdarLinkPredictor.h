

#ifdef ADAMICADARLINKPREDICTOR_EXPORTS
#define ADAMICADARLINKPREDICTOR_API __declspec(dllexport) 
#else
#define ADAMICADARLINKPREDICTOR_API __declspec(dllimport) 
#endif

#ifndef ADAMICADARLINKPREDICTOR_H
#define ADAMICADARLINKPREDICTOR_H

#include "WeightedNetwork.h"
#include "LinkPredictor.h"

class AdamicAdarLinkPredictor : public LinkPredictor {
	private:
	protected:
	public:
		__declspec(dllexport) AdamicAdarLinkPredictor( const WeightedNetwork& );
		__declspec(dllexport) ~AdamicAdarLinkPredictor();
		__declspec(dllexport) virtual double generateScore( vertex_t, vertex_t );
};

#endif
