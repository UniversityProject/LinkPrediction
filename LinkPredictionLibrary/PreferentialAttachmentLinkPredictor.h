#ifndef PREFERENTIALATTACHMENTLINKPREDICTOR_H
#define PREFERENTIALATTACHMENTLINKPREDICTOR_H

#include "WeightedNetwork.h"
#include "LinkPredictor.h"

class PreferentialAttachmentLinkPredictor : public LinkPredictor {
	private:
	protected:
	public:
		__declspec(dllexport) PreferentialAttachmentLinkPredictor( const WeightedNetwork& );
		__declspec(dllexport) ~PreferentialAttachmentLinkPredictor();
		__declspec(dllexport) double generateScore( vertex_t, vertex_t );
};

#endif
