
#ifdef LINKPREDICTOR_EXPORTS
#define LINKPREDICTOR_API __declspec(dllexport) 
#else
#define LINKPREDICTOR_API __declspec(dllimport) 
#endif


#ifndef LINKPREDICTOR_H
#define LINKPREDICTOR_H

#include <vector>
#include <string.h>
#include "WeightedNetwork.h"

using std::ostream;
using std::vector;

class LinkPredictor {
	private:
		LinkPredictor& operator=( const LinkPredictor& ); ////Learn
		const char * EXTENSTION_PREDICTION = ".pred";
	protected:
		const WeightedNetwork& network;
		vertex_t vertex;
		vertex_t neighbor;
		LinkPredictor();
	public:
		__declspec(dllexport) enum Direction { IN, OUT };
		__declspec(dllexport) LinkPredictor( const WeightedNetwork& );
		__declspec(dllexport) virtual ~LinkPredictor();
		__declspec(dllexport) virtual double generateScore( vertex_t, vertex_t ) = 0;  //Learn
		__declspec(dllexport) void printOutNeighborScores( unsigned int degree, const char* outputFilename);
		__declspec(dllexport) void printInNeighborScores( unsigned int degree, const char* outputFilename);
};

#endif
