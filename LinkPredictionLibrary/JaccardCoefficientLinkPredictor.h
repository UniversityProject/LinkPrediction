#ifndef JACCARDCOEFFICIENTLINKPREDICTOR_H
#define JACCARDCOEFFICIENTLINKPREDICTOR_H

#include "WeightedNetwork.h"
#include "LinkPredictor.h"

class JaccardCoefficientLinkPredictor : public LinkPredictor {
	private:
	protected:
	public:
		__declspec(dllexport) JaccardCoefficientLinkPredictor( const WeightedNetwork& );
		__declspec(dllexport) ~JaccardCoefficientLinkPredictor();
		__declspec(dllexport) double generateScore( vertex_t, vertex_t );
};

#endif
