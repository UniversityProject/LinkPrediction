﻿#pragma once

//#ifdef NETWORKLIBRARY_EXPORTS
#define NETWORKLIBRARY_API __declspec(dllexport) 
//#else
//#define NETWORKLIBRARY_API __declspec(dllimport) 
//#endif

#ifndef WEIGHTEDNETWORK_H
#define WEIGHTEDNETWORK_H

#include <vector>
#include <iostream>
#include <climits>
#include <limits>
#include <cstdlib>
#include <unordered_map>
#include <string>

using std::vector;
using std::pair;
using std::istream;
using std::ostream;
using std::numeric_limits;
using std::string;

typedef unsigned int vertex_t;
typedef double weight_t;
typedef pair<vertex_t, vertex_t> edge_t;
typedef pair<vertex_t, weight_t> neighbor_t;
typedef vector<neighbor_t> neighbor_set_t;	//لیستی از همسایه‌ها
typedef vector<neighbor_set_t> vertex_set_t;
//typedef vector<HashTable> link_set_t;
//typedef vector<vector<pair<vertex_t, weight_t>> test_t;
const vertex_t INVALID_VERTEX = numeric_limits<vertex_t>::max();

class HashTable
{
	 std::unordered_map<string, double> htmap;

public:
	 void put(string key, double value)
	{

		htmap[key] = value;
	}
	 double get(const string key)
	{
		return htmap[key];
	}

};

class WeightedNetwork
{
private:
protected:
	vector<vertex_t> dictionary;						// لیست حاوی گره‌ها
	vertex_set_t outList;								// لیستی شامل یال‌های خارجی‌ گره‌ها
	vertex_set_t inList;								// لیستی شامل یال‌های داخلی گره‌ها
	vector<std::unordered_map<string, double>> linkList;
	unsigned int numEdges;								// در بر دارنده تعدا یال‌های شبکه
	void compact();										// ؟؟ به‌نظرم حضورش بیخودیه
	
	void addEdge(vertex_t, vertex_t, weight_t);			// اضافه کردن یال با در توجه به ساختمان داده و قالب مورد نظر
	void addLink(vertex_t, vertex_t, weight_t);			// اضافه کردن یال با در توجه به ساختمان داده و قالب مورد نظر
	static __declspec(dllexport)  neighbor_set_t intersect(const neighbor_set_t&, const neighbor_set_t&);	// اشتراک گرفتن از دو مجموعه مرتب داده‌شده
	WeightedNetwork();																	// سازنده پیش‌فرض

public:
	__declspec(dllexport) WeightedNetwork(const WeightedNetwork&);											// سازنده
	__declspec(dllexport) WeightedNetwork& operator=(const WeightedNetwork&);
	__declspec(dllexport) ~WeightedNetwork();																	// مخرب
	static __declspec(dllexport) WeightedNetwork readStream(istream&, const char, unsigned int, unsigned int, bool);
	static __declspec(dllexport) WeightedNetwork readWeightedStream(istream&, const char, unsigned int, unsigned int, unsigned int, bool);
	static __declspec(dllexport) WeightedNetwork readNetwork(istream&);
	static __declspec(dllexport) WeightedNetwork readNetworkForGsa(istream&);
	static __declspec(dllexport) WeightedNetwork combine(const WeightedNetwork& network1, const WeightedNetwork& network2);
	__declspec(dllexport) neighbor_set_t commonOutNeighbors(vertex_t, vertex_t) const;
	__declspec(dllexport) neighbor_set_t commonInNeighbors(vertex_t, vertex_t) const;
	__declspec(dllexport) void print(ostream&) const;
	__declspec(dllexport) void printEdgeList(ostream&) const;
	__declspec(dllexport) void printAdjacencyList(ostream&) const;
	__declspec(dllexport) unsigned int vertexCount() const;
	__declspec(dllexport) unsigned int edgeCount() const;
	__declspec(dllexport) const neighbor_set_t& outNeighbors(vertex_t) const;
	__declspec(dllexport) const neighbor_set_t& inNeighbors(vertex_t) const;
	__declspec(dllexport) const vector<vertex_t> findNeighborsWithDegreeTwo(vertex_t) const;
	__declspec(dllexport) const vector<vertex_t> findOutNeighbors(vertex_t, unsigned int) const;
	__declspec(dllexport) const vector<vertex_t> findInNeighbors(vertex_t, unsigned int) const;
	__declspec(dllexport) WeightedNetwork snowballSample(vertex_t, unsigned int) const;
	__declspec(dllexport) WeightedNetwork snowballSampleIn(vertex_t, unsigned int) const;
	__declspec(dllexport) WeightedNetwork randomEdgeSample(double, unsigned int) const;
	__declspec(dllexport) WeightedNetwork undirected() const;
	/*
	این تابع لیست یال‌های خروجی را با یال‌های ورودی جا‌به‌جا می‌کند.
	نتیجه حاصل شبکه‌ای است که از منظر جهت یال‌ها با شبکه مورد نظر معکوس می‌باشد.
	*/
	__declspec(dllexport) WeightedNetwork reverseEdges() const;
	__declspec(dllexport) WeightedNetwork removeEdges(const WeightedNetwork &) const;
	__declspec(dllexport) WeightedNetwork removeEdges(const vector<edge_t>&) const;
	__declspec(dllexport) WeightedNetwork thresholdEdges(weight_t) const;
	__declspec(dllexport) WeightedNetwork removeAmutuals() const;
	__declspec(dllexport) WeightedNetwork removeIsolates() const;
	__declspec(dllexport) bool hasEdge(vertex_t, vertex_t) const;
	__declspec(dllexport) bool hasNode(vertex_t) const;
	__declspec(dllexport) bool hasLink(vertex_t, vertex_t) const;
	__declspec(dllexport) weight_t edgeWeight(vertex_t, vertex_t) const;
	__declspec(dllexport) vertex_t maxOutDegreeVertex() const;
	__declspec(dllexport) vector<WeightedNetwork> wccs() const;
	__declspec(dllexport) vector<WeightedNetwork> sccs() const;
	__declspec(dllexport) unsigned int pseudoDiameter() const;
	__declspec(dllexport) unsigned int countMutuals() const;
	__declspec(dllexport) double freeChoiceMutualityIndex() const;
	__declspec(dllexport) unsigned int outDegree(vertex_t) const;
	__declspec(dllexport) unsigned int inDegree(vertex_t) const;
	__declspec(dllexport) weight_t outVolume(vertex_t) const;
	__declspec(dllexport) weight_t inVolume(vertex_t) const;
	__declspec(dllexport) vertex_t translateExtToInt(vertex_t) const;
	__declspec(dllexport) vertex_t translateIntToExt(vertex_t) const;
	__declspec(dllexport) bool hasEdgeExt(vertex_t, vertex_t) const;
	__declspec(dllexport) weight_t edgeWeightExt(vertex_t, vertex_t) const;
	__declspec(dllexport) unsigned int eccentricity(vertex_t vertex) const;
	__declspec(dllexport) double clusteringCoefficient(vertex_t vertex) const;
	__declspec(dllexport) double clusteringCoefficient() const;
	__declspec(dllexport) vector<double> pageRank(double) const;
	__declspec(dllexport) vector<vector<double> > simRank(double) const;
	__declspec(dllexport) double assortativityCoefficient() const;
	__declspec(dllexport) vector<double> betweenness() const;
	__declspec(dllexport) double closeness(vertex_t) const;
	__declspec(dllexport) vector<double> closeness() const;
	__declspec(dllexport) vector<unsigned int> shortestPathLengths() const;
	__declspec(dllexport) vector<unsigned int> shortestPathLengths(vertex_t vertex) const;
	__declspec(dllexport) vertex_set_t getOutList();
	__declspec(dllexport) vertex_set_t getInList();
	__declspec(dllexport) vector<vertex_t> getVertexes();

};

#endif
