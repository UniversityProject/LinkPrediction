#include <stdlib.h>
#include <iostream>
#include <fstream>

#include "StandardArgs.h"
#include "LableNetwork.h"
#include "pr.h"

using namespace std;

const char * EXTENSION_PR = ".pr";

void usage(const char* binary)
{
	cerr << "Usage: " << binary << " [options] -f sortedLableNet.sprob -o outputFileName precision" << endl;
		//precision num_positive\n";
	cerr << "Options: -h             Print this usage message and exit\n";
}

void auc(LableNet& lableNetwork, double step_size, unsigned int p_count, string filename) {

	//ofstream outfile(filename);
	FILE * outfile;
	outfile = fopen(filename.c_str(), "w+");

	double prob_val;
	unsigned int class_val;
	unsigned long last_tp_count = 0;
	unsigned long last_fp_count = 0;
	unsigned long tp_count = 0;
	unsigned long fp_count = 0;
	double last_prob_val = 1.0;
	int result = 0;
	
	//while (network.peek() != EOF)
	//{
		for (int i = 0; i < lableNetwork.size(); i++)
		{
			prob_val = lableNetwork[i].first;
			class_val = lableNetwork[i].second;
		
		//network >> prob_val >> class_val;
		//network.ignore(numeric_limits<streamsize>::max(), '\n');
		//while ((result = fscanf(stdin, "%lf %u\n", &prob_val, &class_val)) != EOF) {
		if (class_val != 1)
		{
			fp_count++;
		}
		else
		{
			tp_count++;
		}
		if (prob_val < last_prob_val)
		{
			unsigned long interp_count = (unsigned long)(((tp_count - last_tp_count) / step_size));
			if (interp_count > 0)
			{
				double tp_delta = (tp_count - last_tp_count) / (double)interp_count;
				double fp_delta = (fp_count - last_fp_count) / (double)interp_count;

				double lower_tp = last_tp_count;
				double lower_fp = last_fp_count;
				if (lower_tp == 0)
				{
					lower_tp = 0.0000000000000001;
				}
				while (lower_tp < tp_count)
				{
					double upper_tp = lower_tp + tp_delta;
					double upper_fp = lower_fp + fp_delta;

					double lower_recall = lower_tp / p_count;
					double lower_precision = lower_tp / (lower_tp + lower_fp);

					fprintf(stdout, "%.9lf %.9lf\n", lower_recall, lower_precision);
					fprintf(outfile, "%.4lf %.4lf\n", lower_recall, lower_precision);

					//outfile << lower_recall << ";" << lower_precision << endl;

					lower_tp = upper_tp;
					lower_fp = upper_fp;
				}
			}
			last_tp_count = tp_count;
			last_fp_count = fp_count;
		}
		last_prob_val = prob_val;
		}
	//}
	unsigned long interp_count = (unsigned long)(((tp_count - last_tp_count) / step_size));
  	if (interp_count > 0)
	{
		double tp_delta = (tp_count - last_tp_count) / (double)interp_count;
		double fp_delta = (fp_count - last_fp_count) / (double)interp_count;

		double lower_tp = last_tp_count;
		double lower_fp = last_fp_count;

		if (lower_tp == 0)
		{ 
			lower_tp = 0.0000000000000001;
		}

		while (lower_tp < tp_count)
		{
			double upper_tp = lower_tp + tp_delta;
			double upper_fp = lower_fp + fp_delta;

			double lower_recall = lower_tp / p_count;
			double lower_precision = lower_tp / (lower_tp + lower_fp);

			fprintf(stdout, "%.9lf %.9lf\n", lower_recall, lower_precision);
			fprintf(outfile, "%.9lf %.9lf\n", lower_recall, lower_precision);

			//outfile << lower_recall << ";" << lower_precision << endl;

			lower_tp = upper_tp;
			lower_fp = upper_fp;
		}
	}
	fclose(outfile);
	return;
}


void pr::run(string sortedLableNetFile, string outputFilename, double precision) 
{
	string outputFile = string(outputFilename) + EXTENSION_PR;
	ifstream sortedLableNet(sortedLableNetFile);
	double step_size = precision;

	//atoi(argv[optind++]);

	LableNet lableNet = LableNetwork::ReadLableNet(sortedLableNet);
	unsigned long p_count = LableNetwork::CountPositiveLable(lableNet);
	
	auc(lableNet, precision, p_count, outputFile);
}


//-f testtest.sprob 1 > text.pr
int main8(int argc, char* argv[]) {

	char c;
	string outputFilename = "output";	//��� ��?� ����?
	ifstream sortedLableNet;

#pragma region ParseArgument
	while ((c = StandardArgs::getopt(argc, argv, "+hf:o::")) >= 0)
	{
		if (c == 'h')
		{
			usage(argv[0]);
			return 0;
		}
		if (c == 'f')
		{
			string name = optarg;
			sortedLableNet.open(optarg, ios::in);
		}
		else if (c == 'o') {
			outputFilename = optarg;
		}
	}
	if (argc != optind + 1)
	{
		usage(argv[0]);
		return 1;
	}
	double step_size = atof(argv[optind++]);

#pragma endregion

	//atoi(argv[optind++]);
	
	LableNet lableNet = LableNetwork::ReadLableNet(sortedLableNet);
	unsigned long p_count = LableNetwork::CountPositiveLable(lableNet);
	outputFilename = string(outputFilename) + ".pr";
	auc(lableNet, step_size, p_count, outputFilename);
	return 0;
}
