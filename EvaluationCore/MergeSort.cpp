//#include "MergeSort.h"
//
//#include <iostream>
//#include <vector>
//
//using namespace std;
//
//MergeSort::MergeSort(LableNet l)
//{
//	size = l.size();
//	list = l;
//} // end MergeSort Constructor
//
//// split vector, sort subvectors and merge subvectors into sorted vector
//LableNet MergeSort::sort()
//{
//	sortSubVector(0, size - 1); // recursively sort entire vector
//	return list;
//} // end function sort
//
//// recursive function to sort subvectors                
//void MergeSort::sortSubVector(int low, int high)
//{
//	// test base case; size of vector equals 1
//	if ((high - low) >= 1) // if not base case
//	{
//		int middle1 = (low + high) / 2; // calculate middle of vector
//		int middle2 = middle1 + 1; // calculate next element over
//
//		// split vector in half; sort each half (recursive calls)
//		sortSubVector(low, middle1); // first half of vector      
//		sortSubVector(middle2, high); // second half of vector    
//
//		// merge two sorted vectors after split calls return
//		merge(low, middle1, middle2, high);
//	} // end if
//} // end function sortSubVector
//
//// merge two sorted subvectors into one sorted subvector
//void MergeSort::merge(int left, int middle1, int middle2, int right)
//{
//	int leftIndex = left; // index into left subvector              
//	int rightIndex = middle2; // index into right subvector         
//	int combinedIndex = left; // index into temporary working vector
//	LableNet combined(size); // working vector              
//
//	// merge vectors until reaching end of either
//	while (leftIndex <= middle1 && rightIndex <= right)
//	{
//		// place smaller of two current elements into result
//		// and move to next space in vector
//		if (list[leftIndex].first >= list[rightIndex].first)
//			combined[combinedIndex++] = list[leftIndex++];
//		else
//			combined[combinedIndex++] = list[rightIndex++];
//	} // end while
//
//	if (leftIndex == middle2) // if at end of left vector          
//	{
//		while (rightIndex <= right) // copy in rest of right vector
//			combined[combinedIndex++] = list[rightIndex++];
//	} // end if                                                     
//	else // at end of right vector                                   
//	{
//		while (leftIndex <= middle1) // copy in rest of left vector
//			combined[combinedIndex++] = list[leftIndex++];
//	} // end else                                                   
//
//	// copy values back into original vector
//	for (int i = left; i <= right; i++)
//		list[i] = combined[i];
//          
//} // end function merge
//
//// display elements in vector
//void MergeSort::displayElements() const
//{
//	displaySubVector(0, size - 1);
//} // end function displayElements
//
//// display certain values in vector
//void MergeSort::displaySubVector(int low, int high) const
//{
//	// output spaces for alignment
//	for (int i = 0; i < low; i++)
//		cout << "   ";
//
//	// output elements left in vector
//	for (int i = low; i <= high; i++)
//		cout << " " << list[i].first << " : " << list[i].second << endl;
//} // end function displaySubVector
