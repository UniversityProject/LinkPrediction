#include <iostream>

#include "StandardArgs.h"
#include "pr.h"
#include "aupr.h"
#include "auroc.h"
#include "roc.h"
#include "LablingPrediction.h"

using namespace std;

void usage(char *prog_name, const char *more) {
	cerr << more;
	cerr << "usage: " << prog_name << " help [switches] " << endl;
	cerr << "switches:" << endl;
	cerr << "\t [pr    -f sortedLableNetFile.sprob -o outputFilename precision] " << endl;
	cerr << "\t [aupr  -f sortedLableNetFile.sprob -o outputFilename precision] " << endl;
	cerr << "\t [roc   -f sortedLableNetFile.sprob -o outputFilename] "<< endl;
	cerr << "\t [auroc -f sortedLableNetFile.sprob -o outputFilename] " << endl;
	cerr << "\t [lable -n TEST.net -p predict.pred -o outputFilename] " << endl;

	exit(0);
}

char * outputFilename = NULL;
char * sortedLableNetFile = NULL;
char * testNetFile = NULL;
char * predictNetFile = NULL;

double step_size = 0;
int main(int argc, char * argv[])
{
	char c;

	if (strcmp(argv[1], "pr") == 0 || strcmp(argv[1], "aupr") == 0)
	{
		for (int i = 2; i < argc; i++)
		{
			if (argv[i][0] == '-')
			{
				switch (argv[i][1])
				{
				case 'f':
					sortedLableNetFile = argv[i + 1];
					i++;
					break;
				case 'o':
					outputFilename = argv[i + 1];
					i++;
					break;
				default:
					usage(argv[0], "pr: Unknown option\n");
				}
			}
			else
			{
				if (step_size == 0)
				{
					step_size = atof(argv[i]);
				}
				else
				{
					usage(argv[0], "pr: InCorrect Command\n");
				}
			}
		}

		if ((sortedLableNetFile == NULL) || (outputFilename == NULL) || (step_size == 0))
		{
			usage(argv[0], "InCorrect Command\n");
		}

		if (strcmp(argv[1], "pr") == 0)
		{
			pr::run(sortedLableNetFile, outputFilename, step_size);

		}
		else
		{
			aupr::run(sortedLableNetFile, outputFilename, step_size);

		}
		
	}
	else if (strcmp(argv[1], "roc") == 0 || strcmp(argv[1], "auroc") == 0)
	{
		for (int i = 2; i < argc; i++)
		{
			if (argv[i][0] == '-')
			{
				switch (argv[i][1])
				{
				case 'f':
					sortedLableNetFile = argv[i + 1];
					i++;
					break;
				case 'o':
					outputFilename = argv[i + 1];
					i++;
					break;
				default:
					usage(argv[0], "Unknown option\n");
				}
			}
			else
			{
				usage(argv[0], "InCorrect Command\n");
			}
		}

		if ((sortedLableNetFile == NULL) || (outputFilename == NULL))
		{
			usage(argv[0], "InCorrect Command\n");
		}

		if (strcmp(argv[1], "roc") == 0)
		{
			roc::run(sortedLableNetFile, outputFilename);
		}
		else
		{
			auroc::run(sortedLableNetFile, outputFilename);
		}

	}
	else if (strcmp(argv[1], "lable") == 0)
	{
		for (int i = 2; i < argc; i++)
		{
			if (argv[i][0] == '-')
			{
				switch (argv[i][1])
				{
				case 'n':
					testNetFile = argv[i + 1];
					i++;
					break;
				case 'p':
					predictNetFile = argv[i + 1];
					i++;
					break;
				case 'o':
					outputFilename = argv[i + 1];
					i++;
					break;
				default:
					usage(argv[0], "Unknown option\n");
				}
			}
			else
			{
				usage(argv[0], "InCorrect Command\n");
			}
		}
		if ((predictNetFile == NULL) || (testNetFile == NULL) || (outputFilename == NULL))
		{
			usage(argv[0], "InCorrect Command\n");
		}

		LablingPrediction::run(predictNetFile, testNetFile, outputFilename);
	}
	else if (strcmp(argv[1], "help") == 0)
	{
		usage(argv[0], "Help\n");
	}
	else
	{
		usage(argv[0], "Unknown option\n");
	}


	//if (filename == NULL)
	//	usage(argv[0], "No input file has been provided\n");

	const char* testNetFilename = argv[optind++];
	return EXIT_SUCCESS;
}