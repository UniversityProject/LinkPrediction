﻿#include "LableNetwork.h"


void LableNetwork::GenerateLableNet(ifstream& predictedNetworkStream, const WeightedNetwork& testNetwork)
{
	//ofstream outfile("testetesstets.prob");
	tpCount = 0;

	unsigned int vertex = 0;
	unsigned int neighbor = 0;
	double probValue = 0;
	while (predictedNetworkStream.peek() != EOF)
	{

		predictedNetworkStream >> vertex >> neighbor >> probValue;
		predictedNetworkStream.ignore(numeric_limits<streamsize>::max(), '\n');
		if ((int)probValue != probValue)
		{
			cout << scientific;
		}
		else
		{
			cout.unsetf(ios_base::floatfield);
		}
		unsigned int lableValue = (unsigned int)testNetwork.hasEdgeExt(vertex, neighbor);
		if (lableValue == 1)
		{
			this->tpCount++;
		}
		Insert(probValue, lableValue);
		//	outfile << score << " " << lable << "\n";
	}
	//	outfile.close();
}

SortedLableNetwork LableNetwork::SortLableNet(istream & is)
{
	SortedLableNetwork  sortedLableNet;
	ProbValue probValue;
	LableValue lableValue;

	while (is >> probValue >> lableValue)
	{
		sortedLableNet.insert(pair<ProbValue, LableValue>(probValue, lableValue));
	}
	return sortedLableNet;
}
SortedLableNetwork LableNetwork::SortLableNet(LableNet lableNet)
{
	SortedLableNetwork  sortedLableNet;

	ProbValue probValue;
	LableValue lableValue;
	for (int i = 0; i < lableNet.size(); i++)
	{
		probValue = lableNet[i].first;
		if (probValue == 0)
		{
			continue;
		}
		lableValue =  lableNet[i].second;
		sortedLableNet.insert(pair<ProbValue, LableValue>(probValue, lableValue));
	}
	return sortedLableNet;
}

void LableNetwork::WriteSortedLableNetOrderByDescend(string filename)
{
	//todo: تست شود
	ofstream outFile(filename + SORTED_LABLE_NETWORK_EXT);
	
	for (SortedLableNetwork::iterator item = sortedLableNet.end(); item != sortedLableNet.begin(); item--)
	{
		//todo: 
			if (item == sortedLableNet.end())
			{
				continue;
			}
			outFile << item->first << " " << item->second << endl;

		//if (item->first == 0)
		//{
		//	continue;
		//}
		
		
	}
	outFile.close();
}

void LableNetwork::WriteUnsortedLableNet(string filename)
{
	WriteLableNet(filename, unsortedLableNet);
}

unsigned int LableNetwork::GetTPCount()
{
	return tpCount;
}

void LableNetwork::Insert(const ProbValue & probValue, const LableValue & lableValue)
{
	InsertInSortedLableNet(probValue, lableValue);
	InsertInUnsortedLableNet(probValue, lableValue);
}

void LableNetwork::InsertInSortedLableNet(const ProbValue & probValue, const LableValue & lableValue)
{
	sortedLableNet.insert(pair<ProbValue, LableValue>(probValue, lableValue));
}

void LableNetwork::InsertInUnsortedLableNet(const ProbValue & probValue, const LableValue & lableValue)
{
	unsortedLableNet.push_back(make_pair(probValue, lableValue));
}



/*
* خواندن فایل شبکه برچسب‌گذاری‌شده
*/
LableNet LableNetwork::ReadLableNet(istream& is)
{
	ProbValue prob_val;
	LableValue lable_val;
	LableNet list;
	while (is >> prob_val >> lable_val)
	{
		list.push_back(make_pair(prob_val, lable_val));
	}
	return list;
}

void LableNetwork::WriteLableNet(string filename, LableNet lableNet)
{
	//todo: تست شود
	ofstream outFile(filename + UNSORTED_LABLE_NETWORK_EXT);
	for (int i = 0; i < lableNet.size(); i++)
	{
		outFile << lableNet[i].first << " " << lableNet[i].second << endl;
	}
	outFile.close();
}

unsigned long LableNetwork::CountPositiveLable(LableNet& lableNet)
{
	unsigned long positiveCount = 0;
	for (int i = 0; i < lableNet.size(); i++)
	{
		if (lableNet[i].second == 1)
		{
			positiveCount++;
		}
	}
	return positiveCount;
}

unsigned long LableNetwork::CountNegativeLable(LableNet & lableNet)
{
	unsigned long negativeCount = 0;
	for (int i = 0; i < lableNet.size(); i++)
	{
		if (lableNet[i].second == 0)
		{
			negativeCount++;
		}
	}
	return negativeCount;
}

