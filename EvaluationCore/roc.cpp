#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <fstream>

#include "roc.h"
#include "StandardArgs.h"
#include "LableNetwork.h"

using namespace std;
const char * EXTENSION_ROC = ".roc";

void auc(LableNet& lableNetwork, string filename, unsigned long negatives, unsigned long positives) {

	FILE * outfile;
	outfile = fopen(filename.c_str(), "w+");

	double prob_val;
	unsigned int class_val;
	unsigned long tp_count = 0;
	unsigned long fp_count = 0;
	double last_prob_val = 1.1;
	int result = 0;
	//while( (result = fscanf( stdin, "%lf %u\n", &prob_val, &class_val )) != EOF ) 
	for (int i = 0; i < lableNetwork.size(); i++)
	{
		prob_val = lableNetwork[i].first;
		class_val = lableNetwork[i].second;

		if (prob_val < last_prob_val) {
			fprintf(stdout, "%.6lf %.6lf\n", (double)fp_count / (double)negatives, (double)tp_count / (double)positives);
			fprintf(outfile, "%.6lf %.6lf\n", (double)fp_count / (double)negatives, (double)tp_count / (double)positives);

		}
		if (class_val != 1) {
			fp_count++;
		}
		else {
			tp_count++;
		}
		last_prob_val = prob_val;
	}
	fprintf(stdout, "%.6lf %.6lf\n", (double)fp_count / (double)negatives, (double)tp_count / (double)positives);
	fprintf(outfile, "%.6lf %.6lf\n", (double)fp_count / (double)negatives, (double)tp_count / (double)positives);
	return;
}

static void usage(const char* binary) {
	//fprintf( stderr, "Usage: %s [options] num_negative num_positive\n", binary );
	fprintf(stderr, "Usage: %s [options] -f sortedLableNet.sprob -o outputFileName\n", binary);
	fprintf(stderr, "Options: -h             Print this usage message and exit\n");
}

void roc::run(string sortedLableNetFile, string outputFilename)
{
	string outputFile = string(outputFilename) + EXTENSION_ROC;
	ifstream sortedLableNet(sortedLableNetFile);
	
	LableNet lableNet = LableNetwork::ReadLableNet(sortedLableNet);
	unsigned long positiveCount = LableNetwork::CountPositiveLable(lableNet);
	unsigned long negativeCount = LableNetwork::CountNegativeLable(lableNet);

	auc(lableNet, outputFile, negativeCount, positiveCount);

}



int main3(int argc, char* argv[]) {
	char c;
	string outputFilename = "output";	//��� ��?� ����?
	ifstream sortedLableNet;

#pragma region ParseArgument

	while ((c = StandardArgs::getopt(argc, argv, "+hf:o::")) >= 0) {
		if (c == 'h') {
			usage(argv[0]);
			return 0;
		}
		if (c == 'f')
		{
			sortedLableNet.open(optarg, ios::in);
		}
		else if (c == 'o') {
			outputFilename = optarg;
		}
	}
	//if( argc != optind + 2 ) {
	//	usage( argv[0] );
	//	return 1;
	//}  
#pragma endregion

	unsigned long negative = atol(argv[optind++]);
	//unsigned long positive = atol( argv[optind++] );

	LableNet lableNet = LableNetwork::ReadLableNet(sortedLableNet);
	unsigned long positiveCount = LableNetwork::CountPositiveLable(lableNet);
	unsigned long negativeCount = LableNetwork::CountNegativeLable(lableNet);

	auc(lableNet, outputFilename, negativeCount, positiveCount);
	return 0;
}

