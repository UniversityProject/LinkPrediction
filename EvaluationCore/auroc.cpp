
#include <stdlib.h>
#include <stdio.h>

#include "auroc.h"
#include "StandardArgs.h"
#include "LableNetwork.h"

using namespace std;
const char * EXTENSION_AUROC = ".auroc";

double auc(LableNet& lableNetwork) {

	double prob_val;
	unsigned int class_val;
	double area = 0.0;
	double cum_neg = 0.0;
	unsigned long last_tp_count = 0;
	unsigned long last_fp_count = 0;
	unsigned long tp_count = 0;
	unsigned long fp_count = 0;
	double last_prob_val = 1.1;
	double cip = 0.0;
	double cin = 0.0;
	int result = 0;
	//while( (result = fscanf( stdin, "%lg %u\n", &prob_val, &class_val )) != EOF ) 
	for (int i = 0; i < lableNetwork.size(); i++)
	{
		prob_val = lableNetwork[i].first;
		class_val = lableNetwork[i].second;

		if( prob_val < last_prob_val ) {
			cip = tp_count - last_tp_count;
			cin = fp_count - last_fp_count;
//			fprintf( stdout, "FP: %lu - TP: %lu - lFP: %lu - lTP: %lu - cip: %lf - cin: %lf - cum_neg: %lf\n", fp_count, tp_count, last_fp_count, last_tp_count, cip, cin, cum_neg );
			area += cip * (cum_neg + (0.5*cin));
			cum_neg += cin;
			last_tp_count = tp_count;
			last_fp_count = fp_count;
//			fprintf( stdout, "Area: %lf\n", area );
		}
		if( class_val != 1 ) {
			fp_count++;
		} else {
			tp_count++;
		}
		last_prob_val = prob_val;
	}
//	fprintf( stdout, "False Positives: %lu - True Positives: %lu\n", fp_count, tp_count );
	cip = tp_count - last_tp_count;
	cin = fp_count - last_fp_count;
//	fprintf( stdout, "FP: %lu - TP: %lu - lFP: %lu - lTP: %lu - cip: %lf - cin: %lf - cum_neg: %lf\n", fp_count, tp_count, last_fp_count, last_tp_count, cip, cin, cum_neg );
	area += cip * (cum_neg + (0.5*cin));
	cum_neg += cin;
	last_tp_count = tp_count;
	last_fp_count = fp_count;
	
	area /= ((unsigned long long)fp_count*tp_count);
	return 1 - area;
}

static void usage( const char* binary ) {
	//fprintf( stderr, "Usage: %s [options]\n", binary );
	fprintf(stderr, "Usage: %s [options] -f sortedLableNet.sprob \n", binary);
	fprintf( stderr, "Options: -h             Print this usage message and exit\n" );
}

void auroc::run(string sortedLableNetFile, string outputFilename) {
	string outputFile = string(outputFilename) + EXTENSION_AUROC;

	ifstream sortedLableNet(sortedLableNetFile);

	LableNet lableNet = LableNetwork::ReadLableNet(sortedLableNet);

	double area = auc(lableNet);

	FILE * outfile;
	outfile = fopen(outputFile.c_str(), "w+");

	fprintf(stdout, "%.9lf\n", area);
	fprintf(outfile, "%9lf\n", area);
}



int main4( int argc, char* argv[] ) {
	char c;
	string outputFilename = "output";	//��� ��?� ����?
	ifstream sortedLableNet;

	while( (c = StandardArgs::getopt( argc, argv, "+hf:o::" )) >= 0 ) {
		if( c == 'h' ) {
			usage( argv[0] );
			return 0;
		}
		if (c == 'f')
		{
			sortedLableNet.open(optarg, ios::in);
		}
		else if (c == 'o') {
			outputFilename = optarg;
		}
	}
	if( argc != optind ) {
		usage( argv[0] );
		return 1;
	}
	LableNet lableNet = LableNetwork::ReadLableNet(sortedLableNet);

	double area = auc(lableNet);
	fprintf( stdout, "%.9lf\n", area );
	return 0;
}

