﻿#pragma once

#ifndef LABLINGPREDICTION
#define LABLINGPREDICTION

#include <vector>
#include <iostream>
#include <fstream>
#include <map>

#include "WeightedNetwork.h"

using namespace std;
typedef double ProbValue;
typedef unsigned int LableValue; 
typedef pair<ProbValue, LableValue> Row;
typedef vector<Row> LableNet;

//map --> sort 
typedef multimap<ProbValue, LableValue> SortedLableNetwork;

const string UNSORTED_LABLE_NETWORK_EXT = ".prob";
const string SORTED_LABLE_NETWORK_EXT = ".sprob";

class LableNetwork
{
private:
	/*
	*تعداد برچسب‌های مثبت
	*/
	unsigned int tpCount = 0;
	/*
	*شبکه برچسب‌زده‌شده مرتب
	*/
	SortedLableNetwork  sortedLableNet;
	/*
	*شبکه برچسب‌زده‌شده نامرتب
	*/
	LableNet unsortedLableNet;

	/*
	*اضافه کردن در شبکه برچسب‌زده‌شده مرتب
	*/
	void InsertInSortedLableNet(const ProbValue & probValue, const LableValue & lableValue);
	/*
	*اضافه کردن در شبکه برچسب‌زده‌شده نامرتب
	*/
	void InsertInUnsortedLableNet(const ProbValue & probValue, const LableValue & lableValue);
	/*
	*اضافه کردن در هر دو شبکه برچسب‌زده‌شده مرتب و نامرتب
	*/
	void Insert(const ProbValue & probValue, const LableValue & lableValue);
public:
	/*
	* تولید شبکه برچسب‌زده شده به صورت مرتب و نامرتب
	*/
	void GenerateLableNet(ifstream& predictedNetworkStream, const WeightedNetwork& testNetwork);
	/*
	*تعداد برچسب‌های مثبت شبکه برچسب‌زده‌شده
	*/
	unsigned int GetTPCount();
	/*
	*نوشتن شبکه‌ برچسب‌زده شده مرتب‌شده به صورت نزولی بر روی دیسک و در مسیر مشخص‌شده
	*پسوند فایل : sprob
	*/
	void WriteSortedLableNetOrderByDescend(string filename);
	/*
	*نوشتن شبکه‌ برچسب‌زده شده نامرتب بر روی دیسک و در مسیر مشخص‌شده
	*پسوند فایل : prob
	*/
	void LableNetwork::WriteUnsortedLableNet(string filename);
	/*
	*خواندن شبکه برچسب‌زده شده از طریق استریم فایل آن
	*/
	static LableNet ReadLableNet(istream& is);
	/*
	*نوشتن شبکه‌ برچسب‌زده شده بر روی دیسک و در مسیر مشخص‌شده
	*پسوند فایل : prob
	*/
	static void WriteLableNet(string filename, LableNet lableNet);
	/*
	مرتب کردن شبکه برچسب‌زده شده با استفاده از آدرس فایل‌ آن
	*/
	static SortedLableNetwork SortLableNet(istream& is);
	/*
	*مرتب کردن شبکه برچسب‌زده شده
	*/
	static SortedLableNetwork SortLableNet(LableNet lableNet);
	
	/*
	*تعداد برچسب‌های مثبت شبکه برچسب‌زده‌شده
	*/
	static unsigned long CountPositiveLable(LableNet&);
	/*
	*تعداد برچسب‌های منفی شبکه برچسب‌زده‌شده
	*/
	static unsigned long CountNegativeLable(LableNet&);

};

#endif