#pragma once
#include <string>
using namespace std;

class LablingPrediction
{
public:
	static void run(string predictNetFile, string testNetFile, string outputFilename);
};
