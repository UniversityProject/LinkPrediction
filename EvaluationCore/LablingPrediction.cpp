﻿#include "StandardArgs.h"
#include "LableNetwork.h"
#include "LablingPrediction.h"

static void usage(const char* binary)
{
	cerr << "Usage: " << binary << " [options] -n predict.pred -o outputFilename TEST.net\n";
	cerr << "Options: -h             Print this usage message and exit\n";
}

void testDataSet()
{

	//برای تست تعداد لینک‌های جدید در دیتاست
	string trainNetFilename = "UNSUPERVISED.net";
	string testNetFilename = "TEST.net";

	ifstream trainFile;
	trainFile.open(trainNetFilename);
	WeightedNetwork trainNetwork = WeightedNetwork::readNetwork(trainFile);
	trainFile.close();

	unsigned int trainVertextCount = trainNetwork.vertexCount();
	//bool flag = trainNetwork.hasEdge(trainVertextCount - 1, trainVertextCount - 1);

	ifstream testFile;
	testFile.open(testNetFilename);
	WeightedNetwork testNetwork = WeightedNetwork::readNetwork(testFile);
	testFile.close();

	// گرفتن لیست لینک‌های خروجی شبکه تست
	vertex_set_t linkList = testNetwork.getOutList();

	//شمارش تعداد لینک‌های جدید و متمایز شبکه تست
#pragma region CountTypesLink
	unsigned int countOlderLink = 0;				// تعداد لینک‌های قدیمی
	unsigned int countNewLinkWithNewNode = 0;		// تعداد لینک‌های جدید با گره‌های جدید
	unsigned int countNewLinkWithOlderNode = 0;		// تعداد لینک‌های جدید با گره‌های قبلی و قدیمی
	for (unsigned int i = 0; i < linkList.size(); i++)
	{
		for (unsigned int j = 0; j < linkList[i].size(); j++)
		{
			if ((i >= trainVertextCount) || (j >= trainVertextCount))
			{
				countNewLinkWithNewNode++;
				continue;
			}
			else
			{
				if (trainNetwork.hasEdge(i, j))
				{
					countOlderLink++;
					continue;
				}
				else
				{
					countNewLinkWithOlderNode++;
					continue;
				}
			}
		}
	}
	cout << "countOlderLink: " << countOlderLink << endl;
	cout << "countNewLinkWithOlderNode: " << countNewLinkWithOlderNode << endl;
	cout << "countNewLinkWithNewNode: " << countNewLinkWithNewNode << endl;
#pragma endregion

}

//facebookTRAIN_DDGSA:1030 //facebookTRAIN_GSA:1030
//-n net.pred TEST.net
//-n AdamicAdar.pred  -o disease $(SolutionDir)\DataSet\disease-g\TEST.net
void LablingPrediction::run(string predictNetFile, string testNetFile, string outputFilename) {
	//testDataSet();
	char c;

	ifstream predictedNetworkStream(predictNetFile);    //استریم net.pred
	string testNetFilename = testNetFile;
	string outputFile = outputFilename;					//نام فایل خروجی

	//خواندن شبکه تست
#pragma region ReadTestNetwork
	ifstream file;
	file.open(testNetFilename);
	WeightedNetwork testNetwork = WeightedNetwork::readNetwork(file);
	file.close();
#pragma endregion

	//تولید و نوشتن شبکه برچسب‌زده مرتب‌شده به صورت نزولی در فایل 
#pragma region CreateSortedLableNet
	LableNetwork lableNetwork;
	lableNetwork.GenerateLableNet(predictedNetworkStream, testNetwork);
	lableNetwork.WriteSortedLableNetOrderByDescend(outputFile);
#pragma endregion


	cout << "TP count: " << lableNetwork.GetTPCount() << endl;

}


//facebookTRAIN_DDGSA:1030 //facebookTRAIN_GSA:1030
//-n net.pred TEST.net
//-n AdamicAdar.pred  -o disease $(SolutionDir)\DataSet\disease-g\TEST.net
int main1(int argc, char* argv[]) {
	//testDataSet();
	char c;

	string outputFilename = "output";	//نام فایل خروجی
	ifstream predictedNetworkStream;    //استریم net.pred

	//تجزیه پارامترهای ورودی
#pragma region Parse Argument

	while ((c = StandardArgs::getopt(argc, argv, "+hn:o::")) >= 0)
	{
		if (c == 'h')
		{
			usage(argv[0]);
			return 0;
		}
		else if (c == 'n') {
			predictedNetworkStream.open(optarg, ios::in);
		}
		else if (c == 'o') {
			outputFilename = optarg;
		}
	}

	if (argc != optind + 1)
	{
		usage(argv[0]);
		return 1;
	}

	const char* testNetFilename = argv[optind++];

#pragma endregion

	//خواندن شبکه تست
#pragma region ReadTestNetwork
	ifstream file;
	file.open(testNetFilename);
	WeightedNetwork testNetwork = WeightedNetwork::readNetwork(file);
	file.close();
#pragma endregion

	//تولید و نوشتن شبکه برچسب‌زده مرتب‌شده به صورت نزولی در فایل 
#pragma region CreateSortedLableNet
	LableNetwork lableNetwork;
	lableNetwork.GenerateLableNet(predictedNetworkStream, testNetwork);
	lableNetwork.WriteSortedLableNetOrderByDescend(outputFilename);
#pragma endregion


	cout << "TP count: " << lableNetwork.GetTPCount() << endl;

	cout << "finsih please enter any key" << endl;
	cin >> c;
	return 0;
}

