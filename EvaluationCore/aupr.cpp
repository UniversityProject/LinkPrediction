#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include "aupr.h"
#include "StandardArgs.h"
#include "LableNetwork.h"

using namespace std;
const char * EXTENSION_AUPR = ".aupr";

double auc(LableNet& lableNetwork, double step_size, unsigned int p_count) {
	double prob_val;
	unsigned int class_val;
	double area = 0.0;
	unsigned long last_tp_count = 0;
	unsigned long last_fp_count = 0;
	unsigned long tp_count = 0;
	unsigned long fp_count = 0;
	double last_prob_val = 1.0;
	int result = 0;
	//while( (result = fscanf( stdin, "%lg %u\n", &prob_val, &class_val )) != EOF )
	for (int i = 0; i < lableNetwork.size(); i++)
	{
		prob_val = lableNetwork[i].first;
		class_val = lableNetwork[i].second;
		if( class_val != 1 ) {
			fp_count++;
		} else {
			tp_count++;
		}
		if( prob_val < last_prob_val ) {
			unsigned long interp_count = (unsigned long)(((tp_count - last_tp_count) / step_size) );
			if( interp_count > 0 ) {
//				fprintf( stderr, "prob_val: %.3lf - tp_count: %lu - fp_count: %lu - interp_count: %lu\n", prob_val, tp_count, fp_count, interp_count );
				double tp_delta = (tp_count - last_tp_count) / (double)interp_count;
				double fp_delta = (fp_count - last_fp_count) / (double)interp_count;
	
				double lower_tp = last_tp_count;
				double lower_fp = last_fp_count;
				if( lower_tp == 0 ) {
					lower_tp = 0.0000000000000001;
				}
				while( lower_tp < tp_count ) {
					double upper_tp = lower_tp + tp_delta;
					double upper_fp = lower_fp + fp_delta;
					
					double lower_recall = lower_tp / p_count;
					double lower_precision = lower_tp / (lower_tp+lower_fp);
//					fprintf( stderr, "prob_val: %.3lf - class_val: %u - tp: %.3lf - fp: %.3lf - r: %.3lf - p: %.3lf\n", prob_val, class_val, lower_tp, lower_fp, lower_recall, lower_precision );
					double upper_recall = upper_tp / p_count;
					double upper_precision = upper_tp / (upper_tp+upper_fp);
					double rectangle_area = (upper_recall-lower_recall)*lower_precision;
					double triangle_area = 0.5*(upper_recall-lower_recall)*(upper_precision-lower_precision);
//					fprintf( stderr, "Rectangle Area: %.3lf - Triangle Area: %.3lf\n", rectangle_area, triangle_area );
					area += rectangle_area + triangle_area;
					
					lower_tp = upper_tp;
					lower_fp = upper_fp;
				}
			}
			last_tp_count = tp_count;
			last_fp_count = fp_count;
		}
		last_prob_val = prob_val;
	}
//	fprintf( stdout, "False Positives: %lu - True Positives: %lu\n", fp_count, tp_count );
	unsigned long interp_count = (unsigned long)(((tp_count - last_tp_count) / step_size) );
	if( interp_count > 0 ) {
//		fprintf( stderr, "prob_val: %.3lf - tp_count: %lu - fp_count: %lu - interp_count: %lu\n", prob_val, tp_count, fp_count, interp_count );
		double tp_delta = (tp_count - last_tp_count) / (double)interp_count;
		double fp_delta = (fp_count - last_fp_count) / (double)interp_count;
	
		double lower_tp = last_tp_count;
		double lower_fp = last_fp_count;
		if( lower_tp == 0 ) {
			lower_tp = 0.0000000000000001;
		}
		while( lower_tp < tp_count ) {
			double upper_tp = lower_tp + tp_delta;
			double upper_fp = lower_fp + fp_delta;
					
			double lower_recall = lower_tp / p_count;
			double lower_precision = lower_tp / (lower_tp+lower_fp);
//			fprintf( stderr, "prob_val: %.3lf - class_val: %u - tp: %.3lf - fp: %.3lf - r: %.3lf - p: %.3lf\n", prob_val, class_val, lower_tp, lower_fp, lower_recall, lower_precision );
			double upper_recall = upper_tp / p_count;
			double upper_precision = upper_tp / (upper_tp+upper_fp);
			double rectangle_area = (upper_recall-lower_recall)*lower_precision;
			double triangle_area = 0.5*(upper_recall-lower_recall)*(upper_precision-lower_precision);
//			fprintf( stderr, "Rectangle Area: %.3lf - Triangle Area: %.3lf\n", rectangle_area, triangle_area );
			area += rectangle_area + triangle_area;
					
			lower_tp = upper_tp;
			lower_fp = upper_fp;
		}
	}
	
//	fprintf( stdout, "FP: %lu - TP: %lu - lFP: %lu - lTP: %lu - cip: %lf - cin: %lf - cum_neg: %lf\n", fp_count, tp_count, last_fp_count, last_tp_count, cip, cin, cum_neg );

	return area;
}

static void usage( const char* binary ) {
	//fprintf( stderr, "Usage: %s [options] precision num_positive\n", binary );
	fprintf( stderr, "Usage: %s [options] -f sortedLableNet.sprob -o outputFileName  precision \n", binary );
	fprintf( stderr, "Options: -h             Print this usage message and exit\n" );
}

void aupr::run(string sortedLableNetFile, string outputFilename, double precision)
{
	string outputFile = string(outputFilename) + EXTENSION_AUPR;
	ifstream sortedLableNet(sortedLableNetFile);
	double step_size = precision;

	LableNet lableNet = LableNetwork::ReadLableNet(sortedLableNet);
	unsigned long p_count = LableNetwork::CountPositiveLable(lableNet);

	/*unsigned long p_count = atoi( argv[optind++] );*/
	double area = auc(lableNet, step_size, p_count);
	FILE * outfile;
	outfile = fopen(outputFile.c_str(), "w+");

	fprintf(stdout, "%.9lf\n", area);
	fprintf(outfile, "%.9lf\n", area);

}



int main5( int argc, char* argv[] ) {
	char c;
	string outputFilename = "output";	//��� ��?� ����?
	ifstream sortedLableNet;

	while( (c = StandardArgs::getopt( argc, argv, "+hf:o::" )) >= 0 ) {
		if( c == 'h' ) {
			usage( argv[0] );
			return 0;
		}
		if (c == 'f')
		{
			sortedLableNet.open(optarg, ios::in);
		}
		else if (c == 'o') {
			outputFilename = optarg;
		}
	}
	if( argc != optind + 1 ) {
		usage( argv[0] );
		return 1;
	}
	double step_size = atof( argv[optind++] );

	LableNet lableNet = LableNetwork::ReadLableNet(sortedLableNet);
	unsigned long p_count = LableNetwork::CountPositiveLable(lableNet);

	/*unsigned long p_count = atoi( argv[optind++] );*/
	double area = auc(lableNet, step_size, p_count);
	fprintf( stdout, "%.9lf\n", area );
	return 0;
}

