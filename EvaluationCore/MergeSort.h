//#ifndef MERGESORT
//#define MERGESORT
//#include <vector>
//#include <fstream>
//
//#include "LableNetwork.h"
//
//using namespace std;
//
//class MergeSort
//{
//public:
//	MergeSort(LableNet); // constructor initializes vector
//	LableNet sort(); // sort vector using merge sort
//	void displayElements() const; // display vector elements
//private:
//	int size; // vector size
//	//vector< int > data; // vector of ints
//	LableNet list;
//	void sortSubVector(int, int); // sort subvector
//	void merge(int, int, int, int); // merge two sorted vectors
//	void displaySubVector(int, int) const; // display subvector
//}; // end class SelectionSort
//
//#endif